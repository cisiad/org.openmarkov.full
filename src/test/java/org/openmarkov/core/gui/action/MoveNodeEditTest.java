package org.openmarkov.core.gui.action;


import static org.junit.Assert.assertEquals;

import java.awt.geom.Point2D;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.openmarkov.core.gui.graphic.VisualChanceNode;
import org.openmarkov.core.gui.graphic.VisualDecisionNode;
import org.openmarkov.core.gui.graphic.VisualNode;
import org.openmarkov.core.model.network.NodeType;
import org.openmarkov.core.model.network.ProbNet;
import org.openmarkov.core.model.network.Node;
import org.openmarkov.core.model.network.Variable;
import org.openmarkov.core.model.network.type.InfluenceDiagramType;


/**
 * This class tests the action of undoing the movement of nodes.
 *
 * @author mpalacios
 */
public class MoveNodeEditTest {
	/**
	 * Network for testing.
	 */
	private ProbNet probNet = null;

	/**
	 * A whose position will be undone and redone.
	 */
	private Node node1 = null;

	/**
	 * A whose position will be undone and redone.
	 */
	private Node node2 = null;

		
	private Variable variableA;
	private Variable variableB;
	/**
	 * This method creates a network and various nodes and various links.
	 *
	 * @throws Exception if an error occurrs.
	 */
	@Before
	public void setUp() throws Exception {
		
		probNet = new ProbNet( InfluenceDiagramType.getUniqueInstance() );
		probNet.setName("Influence diagram");
		probNet.setComment("Influence diagram for testing");
		variableA = new Variable("A");
		variableB = new Variable("B");
		
		node1 = probNet.addNode(variableA, NodeType.CHANCE);
	
		node1.setCoordinateX(100.0);
		node1.setCoordinateY(150.0);
		node2 = probNet.addNode(variableB, NodeType.DECISION);
		node2.setCoordinateX(57.0);
		node2.setCoordinateY(49.0);
		
		VisualChanceNode visualNodeA = new VisualChanceNode(node1,null);
		VisualDecisionNode visualNodeB = new VisualDecisionNode(node2,null);
		visualNodeA.setTemporalPosition(new Point2D.Double (21,160));
		visualNodeB.setTemporalPosition(new Point2D.Double (101,99));
		
		ArrayList<VisualNode> movedNodes = new ArrayList<VisualNode>(2);
		movedNodes.add(visualNodeA);
		movedNodes.add(visualNodeB);
		
		probNet.getPNESupport().setWithUndo(true);
		MoveNodeEdit moveNodeEdit = new MoveNodeEdit(movedNodes);
			
		probNet.doEdit(moveNodeEdit);
	}


	/**
	 * This method undoes and redoes several times.
	 *
	 * @throws Exception if an error occurrs.
	 */
	@Test
	public final void testUndoRedo() throws Exception {
		
		assertEquals(node1.getCoordinateX(), 21.0, 0.1);
		assertEquals(node1.getCoordinateY(), 160.0, 0.1);
		assertEquals(node2.getCoordinateX(), 101.0, 0.1);
		assertEquals(node2.getCoordinateY(), 99.0, 0.1);
		probNet.getPNESupport().undo();
		assertEquals(node1.getCoordinateX(), 100.0, 0.1);
		assertEquals(node1.getCoordinateY(), 150.0, 0.1);
		assertEquals(node2.getCoordinateX(), 57.0, 0.1);
		assertEquals(node2.getCoordinateY(), 49.0, 0.1);
		probNet.getPNESupport().redo();
		assertEquals(node1.getCoordinateX(), 21.0, 0.1);
		assertEquals(node1.getCoordinateY(), 160.0, 0.1);
		assertEquals(node2.getCoordinateX(), 101.0, 0.1);
		assertEquals(node2.getCoordinateY(), 99.0, 0.1);
		probNet.getPNESupport().undo();
		assertEquals(node1.getCoordinateX(), 100.0, 0.1);
		assertEquals(node1.getCoordinateY(), 150.0, 0.1);
		assertEquals(node2.getCoordinateX(), 57.0, 0.1);
		assertEquals(node2.getCoordinateY(), 49.0, 0.1);
		probNet.getPNESupport().redo();
	}
}
